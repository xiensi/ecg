const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    pages: {
        introduction: {
            entry: "./src/pages/introduction/introduction.js",
            template: "./src/pages/introduction/introduction.html",
            filename: "introduction.html",
            title: "introduction"
        },
        index: {
            entry: "./src/pages/main/main.js",
            template: "./src/pages/main/index.html",
            filename: "index.html",
            title: "index"
        },
        login: {
            entry: "./src/pages/login/myLogin.js",
            template: "./src/pages/login/myLogin.html",
            filename: "myLogin.html",
            title: "logimyLoginn"
        },
        deviceActive: {
            entry: "./src/pages/deviceActive/deviceActive.js",
            template: "./src/pages/deviceActive/deviceActive.html",
            filename: "deviceActive.html",
            title: "deviceActive"
        },
    }
})