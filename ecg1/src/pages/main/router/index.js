import { createRouter, createWebHistory } from 'vue-router'
import home from '../views/HomeView.vue'

const routes = [{
        path: '/',
        name: 'home',
        component: home,
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/AboutView.vue')
    },
    {
        path: '/personalCenter',
        name: 'personalCenter',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/PCView.vue')

    },
    {
        path: '/deviceStatus',
        name: 'deviceStatus',
        component: () =>
            import ( /* webpackChunkName: "deviceStatus" */ '../views/DeviceStatusView.vue')
    },
    {
        path: '/history',
        name: 'history',
        component: () =>
            import ( /* webpackChunkName: "deviceStatus" */ '../views/HistoryView.vue')
    },
    {
        path: '/ecgTest',
        name: 'ecgTest',
        component: () =>
            import ( /* webpackChunkName: "deviceStatus" */ '../views/EcgTestView.vue')
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router